﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePermanentData : Singleton<GamePermanentData>
{
    private Quests currentQuests;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void StartQuest(Quests quest)
    {
        currentQuests = quest;
    }

    public void CompleteQuest()
    {
        currentQuests = Quests.None;
    }
}
