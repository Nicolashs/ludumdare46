﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private Transform itemsParent;
    [SerializeField] private Item[] itemsPrefabs;
    [Space(10)]
    [SerializeField] private List<Transform> spawnPoints;

    public void SetSpawnPoints(List<Transform> transforms)
    {
        spawnPoints = transforms;
    }

    public void SpawnItems()
    {
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            int spawnChance = Random.Range(0, 100);

            if(spawnChance < 70)
            {
                int randomItemIndex = Random.Range(0, itemsPrefabs.Length);

                Instantiate(itemsPrefabs[randomItemIndex], spawnPoints[i].position, Quaternion.identity, itemsParent);
            }
        }
    }
}
