﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Vector3 position = PlayerInventory.Instance.transform.position;
        position.y = 30f;

        transform.position = position;
    }
}
