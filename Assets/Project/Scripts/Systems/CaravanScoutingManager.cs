﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrozenCore;
using MapGen;
using UnityEngine.SceneManagement;

public class CaravanScoutingManager : Singleton<CaravanScoutingManager>
{
    [SerializeField] private CaravanMovement caravanMovement;
    [SerializeField] private CharacterHealth caravanHealth;
    [SerializeField] private EnemySpawner enemySpawner;
    //[Space(10)]
    //[SerializeField] private List<CaravanNode> caravanNodes;

    private Tween startCountdownTween = null;

    private float countdown = 3f;

    private int currentOutpostPathIndex = 0;
    private int currentNodeIndex = 0;

    private List<SafePoint> safepoints;

    public void Initialize()
    {
        caravanMovement.OnArriveAtNode = StartEnemyWaves;
        caravanMovement.OnCompletedCaravan = delegate
        {
            OutpostManager.Instance.OpenOutpost();
            currentOutpostPathIndex++;
            currentNodeIndex = 0;
        };
        caravanHealth.OnDeath = delegate
        {
            SceneManager.LoadScene(2);
        };

        enemySpawner.OnWaveCompleted = delegate
        {
            caravanMovement.StartMoving();
        };

        safepoints = MapGenerator.Instance.safePointList;
        
        StartCaravanCountdown();
    }

    public void StartCaravanCountdown()
    {
        List<Vector3> worldWaypoints = safepoints[currentOutpostPathIndex].GetPathWorldPositions();

        caravanMovement.transform.position = worldWaypoints[0];
        worldWaypoints.RemoveAt(0);

        caravanMovement.SetNodes(worldWaypoints);

        startCountdownTween = new Tween(countdown, 0, countdown, TweenCurvesType.Linear, delegate (float interpolatedValue)
        {
            int intCountdown = Mathf.CeilToInt(interpolatedValue);
        });
        startCountdownTween.OnComplete = delegate
        {
            caravanMovement.StartMoving();
        };
    }

    private void StartEnemyWaves()
    {
        List<Transform> enemiesSpawnTransforms = safepoints[currentOutpostPathIndex].GetEnemySpawnTransformsAtPathIndex(currentNodeIndex + 1);

        enemySpawner.SetSpawnPoints(enemiesSpawnTransforms);
        enemySpawner.StartEnemyWaves();

        currentNodeIndex++;
    }
}
