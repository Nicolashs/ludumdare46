﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemySpawnerWave
{
    [SerializeField] private List<Enemy.EnemyType> enemies;

    public List<Enemy.EnemyType> GetEnemies()
    {
        return enemies;
    }
}
