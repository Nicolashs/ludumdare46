﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrozenCore;

public class EnemySpawner : MonoBehaviour
{
    public System.Action OnWaveCompleted;

    [SerializeField] private float wavesInterval;
    [SerializeField] private float spawnInterval;
    [Space(10)]
    [SerializeField] private Transform enemiesParent;
    [SerializeField] private List<Enemy> enemiesPrefabs;
    [Space(10)]
    [SerializeField] private List<Transform> spawnPoints;
    [SerializeField] private List<EnemySpawnerWave> waves;

    private int currentWaveIndex = 0;

    private List<Enemy> enemies = new List<Enemy>();

    public void SetSpawnPoints(List<Transform> points)
    {
        spawnPoints = points;
    }

    public void StartEnemyWaves()
    {
        EnemySpawnerWave currentWave = waves[currentWaveIndex];

        List<Enemy.EnemyType> enemies = currentWave.GetEnemies();

        float cumulativeSpawnInterval = 0;

        for (int i = 0; i < enemies.Count; i++)
        {
            int enemyIndex = i;
            if(i > 0)
            {
                cumulativeSpawnInterval += spawnInterval;

                new Timer(cumulativeSpawnInterval, delegate
                {
                    SpawnEnemy(enemies[enemyIndex]);
                });
            }
            else
            {
                SpawnEnemy(enemies[enemyIndex]);
            }
        }

        currentWaveIndex++;
    }

    private void SpawnEnemy(Enemy.EnemyType type)
    {
        int enemyIndex = (int)type;

        int spawnPositionIndex = Random.Range(0, spawnPoints.Count);
        Vector3 spawnPosition = spawnPoints[spawnPositionIndex].position;

        Enemy newEnemy = Instantiate(enemiesPrefabs[enemyIndex], spawnPosition, Quaternion.identity, enemiesParent);
        enemies.Add(newEnemy);
        newEnemy.Initialize();

        CharacterHealth enemyHealth = newEnemy.GetComponent<CharacterHealth>();
        enemyHealth.OnDeath += CheckEnemiesAlive;
    }

    private void CheckEnemiesAlive()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].gameObject.activeSelf == true)
                return;
        }

        for (int e = 0; e < enemies.Count; e++)
        {
            Destroy(enemies[e].gameObject);
        }

        enemies.Clear();
        enemies = new List<Enemy>();

        OnWaveCompleted?.Invoke();
    }
}
