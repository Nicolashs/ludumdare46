﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public enum ItemType
    {
        Food,
        Scrap,
        Medicine,
        QuestItem
    }

    [SerializeField] private ItemType type;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("1wwwww1");
        if(other.tag == "Player")
        {
        Debug.Log("2wwwww2");
            PlayerInventory inventory = other.GetComponent<PlayerInventory>();

            inventory.AddItem(type, 1);

            Destroy(gameObject);
        }
    }
}
