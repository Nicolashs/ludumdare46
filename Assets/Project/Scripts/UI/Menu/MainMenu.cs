﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Sprite audioMutedSprite;
    [SerializeField] private Sprite audioUnmutedSprite;
    [Space(10)]
    [SerializeField] private Image muteButtonImage;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void MuteUnmute()
    {
        if(AudioListener.volume == 1)
        {
            muteButtonImage.sprite = audioMutedSprite;

            AudioListener.volume = 0f;
        }
        else
        {
            muteButtonImage.sprite = audioUnmutedSprite;

            AudioListener.volume = 1f;
        }
    }
}
