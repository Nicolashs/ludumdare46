﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OutpostManager : Singleton<OutpostManager>
{
    [SerializeField] private GameObject merchantPanel;

    private bool trading = false;
    
    private void Update()
    {
        if(trading == false)
        {
            if(Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    if(hit.collider.tag == "NPC")
                    {
                        OutpostNPC npc = hit.collider.GetComponent<OutpostNPC>();

                        npc.ShowDialog();
                    }
                    else if(hit.collider.tag == "Merchant")
                    {
                        OpenMerchantPanel();
                    }
                }
            }
        }
    }

    private void Start()
    {
        CloseOutpost();
    }

    public void OpenOutpost()
    {
        gameObject.SetActive(true);
    }

    public void CloseOutpost()
    {
        gameObject.SetActive(false);
    }

    public void OpenMerchantPanel()
    {
        trading = true;
        merchantPanel.SetActive(true);
    }

    public void CloseMerchantPanel()
    {
        trading = false;
        merchantPanel.SetActive(false);
    }

    public void BuyFood()
    {
        int scrapCount = PlayerInventory.Instance.GetScrapCount();

        if(scrapCount > 0)
        {
            PlayerInventory.Instance.AddItem(Item.ItemType.Food, 1);
            PlayerInventory.Instance.AddItem(Item.ItemType.Scrap, -1);
        }
    }

    public void BuyMedicine()
    {
        int scrapCount = PlayerInventory.Instance.GetScrapCount();

        if (scrapCount > 0)
        {
            PlayerInventory.Instance.AddItem(Item.ItemType.Medicine, 1);
            PlayerInventory.Instance.AddItem(Item.ItemType.Scrap, -1);
        }
    }

    public void Heal()
    {
        CharacterHealth caravanHealth = CaravanMovement.Instance.GetComponent<CharacterHealth>();

        if(caravanHealth.IsDamaged())
        {
            int medicineCount = PlayerInventory.Instance.GetMedicineCount();

            if (medicineCount > 0)
            {
                PlayerInventory.Instance.AddItem(Item.ItemType.Medicine, -1);
                caravanHealth.Heal(10);
            }
        }
    }

    public void StartGame()
    {
        if(PlayerInventory.Instance.GetFoodCount() > 0)
        {
            PlayerInventory.Instance.AddItem(Item.ItemType.Food, -1);

            CaravanScoutingManager.Instance.StartCaravanCountdown();

            CloseMerchantPanel();
            CloseOutpost();
        }
    }

    public void GiveUp()
    {
        SceneManager.LoadScene(0);
    }
}
