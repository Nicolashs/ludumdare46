﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FrozenCore;

public class OutpostNPC : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private GameObject canvas;
    [Space(10)]
    [SerializeField] private float dialogDuration;
    [SerializeField] private float dialogFadeInOutDuration;
    [Space(10)]
    [SerializeField] private Quests questGiven;

    private Tween dialogTween;
    private Timer dialogTimer;

    public void ShowDialog()
    {
        if(canvas.activeSelf == false)
        {
            canvas.SetActive(true);
            FadeInDialog();

            if (questGiven != Quests.None)
            {
                GamePermanentData.Instance.StartQuest(questGiven);
            }
        }
    }

    public void FadeInDialog()
    {
        dialogTween = new Tween(0f, 1f, dialogFadeInOutDuration, TweenCurvesType.Linear, delegate (float interpolatedAlpha)
        {
            canvasGroup.alpha = interpolatedAlpha;
        });
        dialogTween.OnComplete = StartDialogTimer;
    }

    public void FadeOutDialog()
    {
        dialogTween = new Tween(1f, 0f, dialogFadeInOutDuration, TweenCurvesType.Linear, delegate (float interpolatedAlpha)
        {
            canvasGroup.alpha = interpolatedAlpha;
        });
        dialogTween.OnComplete = delegate
        {
            canvas.SetActive(false);
        };
    }

    public void StartDialogTimer()
    {
        dialogTimer = new Timer(dialogDuration, FadeOutDialog);
    }
}
