﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryUI : Singleton<InventoryUI>
{
    [SerializeField] private TextMeshProUGUI foodText;
    [SerializeField] private TextMeshProUGUI medicalText;
    [SerializeField] private TextMeshProUGUI scrapText;
    [SerializeField] private TextMeshProUGUI questItemText;

    public void UpdateFoodText(int amount)
    {
        foodText.text = amount.ToString();
    }

    public void UpdateMedicalText(int amount)
    {
        medicalText.text = amount.ToString();
    }

    public void UpdateScrapText(int amount)
    {
        scrapText.text = amount.ToString();
    }

    public void UpdateQuestItemText(int amount)
    {
        questItemText.text = amount.ToString();
    }
}
