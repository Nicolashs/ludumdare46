﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrozenCore;

public class CaravanMovement : Singleton<CaravanMovement>
{
    public System.Action OnArriveAtNode;
    public System.Action OnCompletedCaravan;

    [SerializeField] private List<Vector3> nodesList;

    private int nodeIndex = 0;
    private bool underAttack;

    private Tween movementTween = null;

    [SerializeField] private float travelDuration;

    public List<Animator> animatorList;

    public void SetNodes(List<Vector3> nodes)
    {
        //Debug.Log(nodes.Count);
        nodesList = nodes;

        nodeIndex = 0;
    }

    public void StartMoving()
    {
        Debug.Log("StartMoving");
        underAttack = false;

        transform.LookAt(nodesList[nodeIndex]);
        Vector3 eulerAngles = transform.localEulerAngles;
        eulerAngles.x = 0;
        transform.eulerAngles = eulerAngles;

        AnimationMove();

        movementTween = new Tween(transform.position, nodesList[nodeIndex], travelDuration, TweenCurvesType.Linear, delegate (Vector3 interpolatedPosition)
        {
            transform.position = interpolatedPosition;
        });
        movementTween.OnComplete = delegate
        {
            if (nodeIndex == nodesList.Count - 1)
            {
                Debug.Log("caravan completed");
                OnCompletedCaravan?.Invoke();
            }
            else
            {
                underAttack = true;
                nodeIndex++;

                OnArriveAtNode?.Invoke();
            }

            AnimationStop();
        };
    }

    public void AnimationMove() {
        foreach (Animator anim in animatorList) {
            anim.SetBool("Moving", true);
        }
    }

    public void AnimationStop() {
        foreach (Animator anim in animatorList) {
            anim.SetBool("Moving", false);
        }
    }
}
