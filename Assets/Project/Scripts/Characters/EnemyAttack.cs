﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float damage;

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.collider.tag);
        if (collision.collider.tag == "Caravan")
        {
            CharacterHealth caravanHealth = collision.collider.GetComponent<CharacterHealth>();
            caravanHealth.TakeDamage(damage);

            CharacterHealth enemyHealth = GetComponent<CharacterHealth>();
            enemyHealth.Die();
        }
    }
}
