﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealth : MonoBehaviour
{
    public System.Action OnDeath;

    [SerializeField] private float maximumHealth;
    [SerializeField] private float currentHealth;
    [SerializeField] private float resistance;

    [SerializeField] private Image healthBar;

    private void Start()
    {
        currentHealth = maximumHealth;
        UpdateHealthBar();
    }

    public bool IsDamaged()
    {
        return currentHealth != maximumHealth;
    }

    public void SetMaximumHealth(float maximumHealth)
    {
        this.maximumHealth = maximumHealth;
        currentHealth = maximumHealth;

        UpdateHealthBar();
    }

    public void SetResistance(float resistance)
    {
        this.resistance = resistance;
    }

    public void TakeDamage(float damage)
    {
        float incomingDamage = Mathf.Clamp(damage - resistance, 0, damage);

        //Debug.Log(name + " took " + incomingDamage + " damage");

        currentHealth -= incomingDamage;

        if(currentHealth < 0)
        {
            currentHealth = 0;
        }

        UpdateHealthBar();

        if (currentHealth <= 0)
        {
            OnDeath?.Invoke();
        }
    }

    public void Heal(float amount)
    {
        currentHealth += amount;

        currentHealth = Mathf.Clamp(currentHealth, 0, maximumHealth);

        UpdateHealthBar();
    }

    public void Die()
    {
        currentHealth = 0;

        OnDeath?.Invoke();
    }

    private void UpdateHealthBar()
    {
        if (healthBar == null)
            return;

        float value = currentHealth / maximumHealth;

        healthBar.fillAmount = value;
    }
}
