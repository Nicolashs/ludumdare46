﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caravan : Singleton<Caravan>
{
    public Vector3 GetCaravanPosition()
    {
        return transform.position;
    }
}
