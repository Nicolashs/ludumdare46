﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrozenCore;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private int clipSize;
    [SerializeField] private float shotCooldown;
    //[SerializeField] private float reloadingTime;
    [Space(10)]
    [SerializeField] private PlayerProjectile projectile;

    private Timer cooldownTimer = null;
    //private Timer reloadTimer = null;
    private bool canShoot = true;

    private void Update()
    {
        if (OutpostManager.Instance.gameObject.activeSelf == true)
            return;

        if(Input.GetMouseButtonDown(0))
        {
            if (canShoot == true)
            {
                canShoot = false;

                PlayerProjectile newProjectile = Instantiate(projectile, transform.position + transform.forward, transform.rotation);
                newProjectile.SetDamage(damage);

                cooldownTimer = new Timer(shotCooldown, delegate ()
                {
                    canShoot = true;
                });
            }
        }
    }
}
