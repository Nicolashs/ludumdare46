﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private new Rigidbody rigidbody;
    [SerializeField] private float movementSpeed;

    private void Update()
    {
        UpdateMovement();
        UpdateRotation();
    }

    private void UpdateRotation()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.red);

            transform.LookAt(hit.point);
            Vector3 eulerAngles = transform.localEulerAngles;
            eulerAngles.x = 0;
            transform.eulerAngles = eulerAngles;
        }
    }

    private void UpdateMovement()
    {
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        float verticalMovement = Input.GetAxisRaw("Vertical");

        Vector3 movementVector = new Vector3(horizontalMovement, 0, verticalMovement);

        rigidbody.MovePosition(transform.position + (movementVector * movementSpeed * Time.deltaTime));
    }
}
