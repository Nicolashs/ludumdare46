﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    [SerializeField] private float projectileSpeed;

    private float damage;

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }

    private void Update()
    {
        transform.position += transform.forward * projectileSpeed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            CharacterHealth enemyHealth = other.GetComponent<CharacterHealth>();

            enemyHealth.TakeDamage(damage);

            Destroy(gameObject);
        }
        else if(other.tag == "Obstacle" || other.tag == "Caravan")
        {
            Destroy(gameObject);
        }
    }
}
