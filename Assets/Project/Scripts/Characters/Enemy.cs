﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public enum EnemyType
    {
        Common, Runner, Armored
    }

    [SerializeField] private EnemyType type;
    [Space(10)]
    [SerializeField] private GameObject[] variations;
    [Space(10)]
    [SerializeField] private NavMeshAgent agent;

    public EnemyType GetEnemyType()
    {
        return type;
    }

    public void Initialize()
    {
        CharacterHealth health = GetComponent<CharacterHealth>();

        health.OnDeath += delegate
        {
            gameObject.SetActive(false);
        };

        if(variations.Length > 0)
        {
            for (int i = 0; i < variations.Length; i++)
            {
                variations[i].SetActive(false);
            }

            int randomVariation = Random.Range(0, variations.Length);

            variations[randomVariation].SetActive(true);
        }
    }

    public void Start()
    {
        Vector3 caravanPosition = Caravan.Instance.GetCaravanPosition();

        agent.SetDestination(caravanPosition);
    }
}
