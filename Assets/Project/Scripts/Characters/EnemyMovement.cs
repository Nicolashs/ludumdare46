﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [Space(10)]
    [SerializeField] private NavMeshAgent agent;

    private void Start()
    {
        agent.speed = movementSpeed;
    }
}
