﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : Singleton<PlayerInventory>
{
    private Dictionary<Item.ItemType, int> itemStacks;

    private void Start()
    {
        itemStacks = new Dictionary<Item.ItemType, int>();

        itemStacks.Add(Item.ItemType.Food, 0);
        itemStacks.Add(Item.ItemType.Medicine, 0);
        itemStacks.Add(Item.ItemType.Scrap, 5);
        itemStacks.Add(Item.ItemType.QuestItem, 0);

        UpdateInventoryText();
    }

    public void AddItem(Item.ItemType itemType, int quantity)
    {
        itemStacks[itemType] += quantity;

        UpdateInventoryText();
    }

    public void UpdateInventoryText()
    {
        InventoryUI.Instance.UpdateFoodText(itemStacks[Item.ItemType.Food]);

        InventoryUI.Instance.UpdateScrapText(itemStacks[Item.ItemType.Scrap]);

        InventoryUI.Instance.UpdateMedicalText(itemStacks[Item.ItemType.Medicine]);

        InventoryUI.Instance.UpdateQuestItemText(itemStacks[Item.ItemType.QuestItem]);
    }

    public int GetScrapCount()
    {
        return itemStacks[Item.ItemType.Scrap];
    }

    public int GetFoodCount()
    {
        return itemStacks[Item.ItemType.Food];
    }

    public int GetMedicineCount()
    {
        return itemStacks[Item.ItemType.Medicine];
    }
}
