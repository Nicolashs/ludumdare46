﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen
{
    public class SafePoint : MonoBehaviour
    {
        public bool connectedIn, connectedOut, placed;
        public Vector2Int positionalId = Vector2Int.down;
        public int minSafepointDistance = 3;
        public bool isFirstSafepoint;
        public bool isLastSafepoint;
        public int index;
        public int cellsUsed = 1;
        public List<Vector2Int> outPath = new List<Vector2Int>();

        //Debug
        public Color color;
        public float offset;

        public ReflectionProbe reflectionProbe;
        Cubemap reflectionCubemap;

        private void OnEnable() {
            this.color = UnityEngine.Random.ColorHSV(0, 1, 1, 1, 1, 1);
            this.offset = UnityEngine.Random.Range(-2.5f, 2.5f);
        }

        private void Start() {
            reflectionCubemap = new Cubemap(reflectionProbe.resolution, UnityEngine.Experimental.Rendering.DefaultFormat.LDR, UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
            reflectionProbe.customBakedTexture = reflectionCubemap;
            reflectionProbe.RenderProbe();
        }

        public List<Vector3> GetPathWorldPositions() {
            List<Vector3> result = new List<Vector3>();

            foreach (Vector2Int position in outPath) {
                result.Add(new Vector3(MapGenerator.Instance.GetWorldAxisValue(position.x), 0, MapGenerator.Instance.GetWorldAxisValue(position.y)));
            }

            return result;
        }

        public List<Transform> GetEnemySpawnTransformsAtPathIndex(int index) {
            Street street = MapGenerator.Instance.GetStreetByPosition(outPath[index]);

            return street.enemySpawnList;
        }

        public List<Transform> GetItemSpawnTransformsAtPathIndex(int index) {
            return MapGenerator.Instance.GetStreetByPosition(outPath[index]).itemSpawnList;
        }
    }
}
