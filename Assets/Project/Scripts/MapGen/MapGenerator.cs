﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace MapGen
{
    public enum StreetType
    {
        CrossStreet = 0,
        TStreet,
        LStreet
    }

    public class MapGenerator : Singleton<MapGenerator>
    {
        const int EMPTY_TILE_ID = -999;
        const int SAFEPOINT_ID_OFFSET = 100;
        const int STREET_TILE_ID = 1;
        const int HOUSE_TILE_ID = 2;

        public Vector2Int mapResolution;
        public float positioningScale = 10, positioningOffset = 5;
        public List<GameObject> safepointPrefabList;
        public List<GameObject> safepointGameObjectList = new List<GameObject>();
        public List<SafePoint> safePointList = new List<SafePoint>();
        int safepointDistance = 3;

        [Header("Prefab References")]
        public GameObject crossStreetPrefab;
        public GameObject straightStreet;
        public GameObject TStreetPrefab;
        public GameObject LStreetPrefab;
        public GameObject gridTilePrefab;
        public GameObject safepointPrefab;
        public List<GameObject> housePrefabList;

        //Internals
        int[,] mapIndexList;
        List<House> houseTileList = new List<House>();
        List<Street> streetTileList = new List<Street>();

        private void Start() {
            safepointDistance = Mathf.FloorToInt(((mapResolution.x < mapResolution.y) ? mapResolution.x : mapResolution.y) * 0.2f);
            mapResolution.x += 1; //Border
            mapResolution.y += 1; //Border
            GenerateMap();

            //Place Floor Collider
            GameObject floorCollider = new GameObject("FloorCollider");
            floorCollider.transform.position = new Vector3(GetWorldAxisValue(mapResolution.x) / 2, -0.5f, GetWorldAxisValue(mapResolution.y) / 2);
            BoxCollider bc = floorCollider.AddComponent<BoxCollider>();
            bc.size = new Vector3(GetWorldAxisValue(mapResolution.x), 1, GetWorldAxisValue(mapResolution.y));

            //Place NavMesh
            GameObject navMeshGO = new GameObject("NavMesh");
            NavMeshSurface nms = navMeshGO.AddComponent<NavMeshSurface>();
            nms.BuildNavMesh();

            CaravanScoutingManager.Instance.Initialize();
        }

        public void GenerateMap() {
            //Initialize indexes
            mapIndexList = new int[mapResolution.x, mapResolution.y];

            for (int y = 0; y < mapResolution.y; y++) {
                for (int x = 0; x < mapResolution.x; x++) {
                    mapIndexList[x, y] = EMPTY_TILE_ID;
                }
            }

            for (int i = 0; i < safepointPrefabList.Count; i++) {
                GameObject newSafepoint = Instantiate(safepointPrefabList[i]);
                newSafepoint.name = "Safepoint-" + i;
                safepointGameObjectList.Add(newSafepoint);
                newSafepoint.GetComponent<SafePoint>().index = i;
                safePointList.Add(newSafepoint.GetComponent<SafePoint>()); //Can pass the safepoint data here for setup
            }

            safePointList[0].connectedIn = safePointList[0].isFirstSafepoint = true;
            safePointList[safePointList.Count - 1].connectedOut = safePointList[safePointList.Count - 1].isLastSafepoint = true;

            //Generate safe point positions
            for (int i = 0; i < safePointList.Count; i++) {
                Vector2Int randomPosition = new Vector2Int();
                bool valid = true;
                //Randomize until find a empty tile
                do {
                    valid = true;

                    if (i - 1 == -1) { //First safepoint
                        randomPosition.x = 1;
                        randomPosition.y = 1;
                    }
                    else if (i + 1 == safePointList.Count) { //Last Safepoint
                        randomPosition.x = mapResolution.x - 2;
                        randomPosition.y = mapResolution.y - 2;
                    }
                    else {
                        randomPosition = new Vector2Int(UnityEngine.Random.Range(1, mapResolution.x - 1), Mathf.RoundToInt(i * (mapResolution.y / (safePointList.Count - 1))));

                        if (i % 2 == 0) {
                            if (randomPosition.x > (mapResolution.x / 2)) {
                                valid = false;
                            }
                        }
                        else {
                            if (randomPosition.x < (mapResolution.x / 2)) {
                                valid = false;
                            }
                        }

                        if (!IsEmptyColumn(randomPosition.x) && mapIndexList[randomPosition.x, randomPosition.y] != EMPTY_TILE_ID) {
                            valid = false;
                        }
                    }
                }
                while (!valid);

                mapIndexList[randomPosition.x, randomPosition.y] = SAFEPOINT_ID_OFFSET; //No asset will have a greater id than safepoints
                safePointList[i].positionalId = randomPosition;
                safePointList[i].placed = true;
            }

            //Connect safepoints - find closest safepoint and connect
            SafePoint currentSafepointToConnect = safePointList[0];
            do {
                //Connect safepoints
                currentSafepointToConnect.outPath.Add(currentSafepointToConnect.positionalId);

                Vector2Int currentPosition = currentSafepointToConnect.positionalId;
                Vector2Int destination = safePointList[currentSafepointToConnect.index + 1].positionalId;//nearestSafepoint.position;

                do {
                    Vector2Int newPosition = Vector2Int.zero;

                    Vector2 currentDirection = ((Vector2)destination - (Vector2)currentPosition).normalized;
                    currentDirection.x = Mathf.RoundToInt(currentDirection.x);
                    currentDirection.y = Mathf.RoundToInt(currentDirection.y);

                    if (Mathf.Abs(currentDirection.x) == Mathf.Abs(currentDirection.y)) {
                        if (UnityEngine.Random.Range(0, 100) % 2 == 0) {
                            currentDirection.y = 0;
                        }
                        else {
                            currentDirection.x = 0;
                        }
                    }

                    newPosition = new Vector2Int(currentPosition.x + (int)currentDirection.x, currentPosition.y + (int)currentDirection.y);

                    if (mapIndexList[newPosition.x, newPosition.y] == EMPTY_TILE_ID) {
                        mapIndexList[newPosition.x, newPosition.y] = STREET_TILE_ID;
                        currentSafepointToConnect.outPath.Add(new Vector2Int(newPosition.x, newPosition.y));
                    }
                    else if (mapIndexList[newPosition.x, newPosition.y] == STREET_TILE_ID) {
                        currentSafepointToConnect.outPath.Add(new Vector2Int(newPosition.x, newPosition.y));
                    }

                    currentPosition = newPosition;
                } while (currentPosition != destination);

                currentSafepointToConnect.connectedOut = true;
                currentSafepointToConnect.outPath.Add(safePointList[currentSafepointToConnect.index + 1].positionalId);
                safePointList[currentSafepointToConnect.index + 1].connectedIn = true;
                currentSafepointToConnect = safePointList[currentSafepointToConnect.index + 1];
            } while (!AllSafepointsAreConnected());

            for (int y = 0; y < mapResolution.y; y++) {
                for (int x = 0; x < mapResolution.x; x++) {
                    if (PositionIsInBounds(x, y) && mapIndexList[x, y] == EMPTY_TILE_ID) {
                        mapIndexList[x, y] = (UnityEngine.Random.Range(0, 101) % 60 > 1) ? HOUSE_TILE_ID : STREET_TILE_ID;
                    }
                }
            }

            for (int y = 0; y < mapResolution.y; y++) {
                for (int x = 0; x < mapResolution.x; x++) {
                    GameObject newTile = null;
                    Vector3 position = new Vector3(GetWorldAxisValue(x), 0, GetWorldAxisValue(y));

                    switch (mapIndexList[x, y]) {
                        case EMPTY_TILE_ID:
                            newTile = Instantiate(gridTilePrefab, position, Quaternion.identity);
                            newTile.name = "[" + x + "," + y + "]";
                            newTile.transform.SetParent(this.transform);
                            break;
                        case HOUSE_TILE_ID:
                            if (housePrefabList.Count > 0) {
                                newTile = Instantiate(housePrefabList[UnityEngine.Random.Range(0, housePrefabList.Count)], position, Quaternion.identity);
                                newTile.name = "[" + x + "," + y + "]" + newTile.name;
                                newTile.transform.SetParent(this.transform);
                                newTile.GetComponent<House>().positionalId = new Vector2Int(x, y);
                                houseTileList.Add(newTile.GetComponent<House>());
                            }
                            break;
                        case STREET_TILE_ID:
                            bool up = (mapIndexList[x, y + 1] == STREET_TILE_ID || mapIndexList[x, y + 1] >= SAFEPOINT_ID_OFFSET) ? true : false; //Up
                            bool down = (mapIndexList[x, y - 1] == STREET_TILE_ID || mapIndexList[x, y - 1] >= SAFEPOINT_ID_OFFSET) ? true : false; //Down
                            bool center = (mapIndexList[x, y] == STREET_TILE_ID || mapIndexList[x, y + 1] >= SAFEPOINT_ID_OFFSET) ? true : false; //Center
                            bool left = (mapIndexList[x - 1, y] == STREET_TILE_ID || mapIndexList[x - 1, y] >= SAFEPOINT_ID_OFFSET) ? true : false; //Left
                            bool right = (mapIndexList[x + 1, y] == STREET_TILE_ID || mapIndexList[x + 1, y] >= SAFEPOINT_ID_OFFSET) ? true : false; //Right

                            if (up == false && down == true && left == true && right == true)       //T Street Down
                                newTile = Instantiate(TStreetPrefab, position, Quaternion.Euler(0, 0, 0));
                            else if (up == true && down == false && left == true && right == true)  //T Street Up
                                newTile = Instantiate(TStreetPrefab, position, Quaternion.Euler(0, 180, 0));
                            else if (up == true && down == true && left == true && right == false)   //T Street Left
                                newTile = Instantiate(TStreetPrefab, position, Quaternion.Euler(0, 90, 0));
                            else if (up == true && down == true && left == false && right == true)   //T Street Right
                                newTile = Instantiate(TStreetPrefab, position, Quaternion.Euler(0, 270, 0));
                            else if (up == false && down == true && left == false && right == true)   //L Street Down
                                newTile = Instantiate(LStreetPrefab, position, Quaternion.Euler(0, 270, 0));
                            else if (up == true && down == false && left == true && right == false)   //L Street Up
                                newTile = Instantiate(LStreetPrefab, position, Quaternion.Euler(0, 90, 0));
                            else if (up == false && down == true && left == true && right == false)   //L Street Left
                                newTile = Instantiate(LStreetPrefab, position, Quaternion.Euler(0, 0, 0));
                            else if (up == true && down == false && left == false && right == true)   //L Street Right
                                newTile = Instantiate(LStreetPrefab, position, Quaternion.Euler(0, 180, 0));
                            else if (up == true && down == true && left == false && right == false)   //Straight Street Vertical
                                newTile = Instantiate(straightStreet, position, Quaternion.Euler(0, 0, 0));
                            else if (up == false && down == false && left == true && right == true)   //Straight Street Horizontal
                                newTile = Instantiate(straightStreet, position, Quaternion.Euler(0, 90, 0));
                            else
                                newTile = Instantiate(crossStreetPrefab, position, Quaternion.Euler(0, 0, 0));

                            newTile.name = "[" + x + "," + y + "]";
                            newTile.transform.SetParent(this.transform);
                            newTile.GetComponent<Street>().positionalId = new Vector2Int(x, y);
                            streetTileList.Add(newTile.GetComponent<Street>());
                            break;
                        case SAFEPOINT_ID_OFFSET:
                            int index = GetSafepointIdByPosition(new Vector2Int(x, y));
                            safepointGameObjectList[index].transform.position = position;
                            break;
                    }
                }
            }
        }

        public SafePoint GetSafepointByPosition(Vector2Int position) {
            return safePointList.Find(x => x.positionalId == position);
        }

        public Street GetStreetByPosition(Vector2Int position) {
            return streetTileList.Find(x => x.positionalId == position);
        }

        public House GetHouseByPosition(Vector2Int position) {
            return houseTileList.Find(x => x.positionalId == position);
        }

        int GetSafepointIdByPosition(Vector2Int position) {
            return safePointList.FindIndex(x => x.positionalId == position);
        }

        List<SafePoint> GetSafepointsInArea(Vector2Int position, int area) {
            Vector2Int min, max;
            min = new Vector2Int(position.x - Mathf.FloorToInt(area / 2), position.y - Mathf.FloorToInt(area / 2));
            max = new Vector2Int(position.x + Mathf.FloorToInt(area / 2), position.y + Mathf.FloorToInt(area / 2));

            return safePointList.FindAll(x => ((x.positionalId.x > min.x && x.positionalId.x < max.x) && (x.positionalId.y > min.y && x.positionalId.y < max.y)));
        }

        int CountUsedIdexesInArea(Vector2Int position, int area) {
            int count = 0;
            Vector2Int min, max;
            min = new Vector2Int(position.x - Mathf.FloorToInt(area / 2), position.y - Mathf.FloorToInt(area / 2));
            max = new Vector2Int(position.x + Mathf.FloorToInt(area / 2), position.y + Mathf.FloorToInt(area / 2));
            for (int y = min.y; y < max.y; y++) {
                for (int x = min.x; x < max.x; x++) {
                    if ((x > 0 && x < mapResolution.x) && (y > 0 && y < mapResolution.y)) { //position is inside the map
                        if (mapIndexList[x, y] != EMPTY_TILE_ID) {
                            count++;
                        }
                    }
                }
            }
            return count;
        }

        List<SafePoint> GetPlacedSafepoints() {
            return safePointList.FindAll(x => x.placed == true);
        }

        bool AllSafepointsArePlaced() {
            return safePointList.Count == safePointList.FindAll(x => x.placed == true).Count;
        }

        bool AllSafepointsAreConnected() {
            int count = safePointList.FindAll(x => (x.connectedIn && x.connectedOut)).Count;
            return safePointList.Count == count;
        }

        Vector2Int GetRandomPosition(Vector2Int position) { //Returns an empty position within and radius
            Vector2Int randomPosition = new Vector2Int();
            bool valid = false;
            do {
                valid = true;

                randomPosition.x = UnityEngine.Random.Range(1, mapResolution.x - 1);
                randomPosition.y = UnityEngine.Random.Range(1, mapResolution.y - 1);

                float currentDistance = Vector2.Distance(randomPosition, position);

                if (currentDistance >= safepointDistance) {
                    if (IsEmptyColumn(randomPosition.x) && IsEmptyRow(randomPosition.y)) {
                        valid = true;
                    }
                    else {
                        valid = false;
                    }
                }
                else {
                    valid = false;
                }

            } while (!valid);

            return randomPosition;
        }

        Vector2Int GetRandomPositionInRange(Vector2Int position, int radius) { //Returns an   empty position within and radius
            Vector2Int randomPosition = new Vector2Int();
            bool valid = false;
            do {
                valid = true;

                //Circle Range!
                float randomAngle = UnityEngine.Random.Range(0, 360);

                randomPosition.x = Mathf.RoundToInt(radius * Mathf.Cos(randomAngle));
                randomPosition.y = Mathf.RoundToInt(radius * Mathf.Sin(randomAngle));

                if ((randomPosition.x > 0 && randomPosition.x < mapResolution.x - 1) && (randomPosition.y > 0 && randomPosition.y < mapResolution.y - 1)) {
                    if (IsEmptyColumn(randomPosition.x) && IsEmptyRow(randomPosition.y)) {
                        valid = true;
                    }
                    else {
                        valid = false;
                    }
                }
                else {
                    valid = false;
                }

            } while (!valid);

            return randomPosition;
        }

        bool IsEmptyColumn(int column) {
            for (int i = 0; i < mapResolution.y; i++) {
                if (mapIndexList[column, i] >= SAFEPOINT_ID_OFFSET) {
                    return false;
                }
            }

            return true;
        }

        bool IsEmptyRow(int row) {
            for (int i = 0; i < mapResolution.x; i++) {
                if (mapIndexList[i, row] >= SAFEPOINT_ID_OFFSET) {
                    return false;
                }
            }

            return true;
        }

        bool PositionIsInBounds(int x, int y) {
            if ((x > 0 && x < mapResolution.x - 1) && (y > 0 && y < mapResolution.y - 1))
                return true;
            else
                return false;
        }

        public float GetWorldAxisValue(int axisValue) {
            return (axisValue * positioningScale) + positioningOffset;
        }

        float GetMapAxisValue(float axisValue) {
            return Mathf.RoundToInt((axisValue - positioningOffset) / positioningScale);
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(Vector3.zero, 2); //World center

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(new Vector3(mapResolution.x * positioningScale, 0, mapResolution.y * positioningScale), 2);

            //Paths
            foreach (SafePoint sp in safePointList) {
                Gizmos.color = sp.color;
                for (int i = 0; i < sp.outPath.Count; i++) {
                    if (i + 1 < sp.outPath.Count) {
                        Vector3 from = new Vector3(GetWorldAxisValue(sp.outPath[i].x) + sp.offset, 5, GetWorldAxisValue(sp.outPath[i].y) + sp.offset);
                        Vector3 to = new Vector3(GetWorldAxisValue(sp.outPath[i + 1].x) + sp.offset, 5, GetWorldAxisValue(sp.outPath[i + 1].y) + sp.offset);
                        Gizmos.DrawLine(from, to);
                    }

                    if (i + 1 == sp.outPath.Count) {
                        Gizmos.DrawCube(new Vector3(GetWorldAxisValue(sp.outPath[i].x) + sp.offset, 5, GetWorldAxisValue(sp.outPath[i].y) + sp.offset), Vector3.one * 3);
                    }
                }
            }
        }

        public List<Transform> GetAllItemSpawn() {
            List<Transform> result = new List<Transform>();

            for (int y = 0; y < mapResolution.y; y++) {
                for (int x = 0; x < mapResolution.x; x++) {
                    Vector2Int pos = new Vector2Int(x, y);
                    switch (mapIndexList[x, y]) {
                        case STREET_TILE_ID:
                            foreach (Transform item in GetStreetByPosition(pos).itemSpawnList) {
                                result.Add(item);
                            }
                            break;

                        case HOUSE_TILE_ID:
                            foreach (Transform item in GetHouseByPosition(pos).itemSpawnList) {
                                result.Add(item);
                            }
                            break;
                    }
                }
            }

            return result;
        }
    }
}
