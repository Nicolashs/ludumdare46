﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen
{
    public class Pattern
    {
        public string pattern;
        public StreetType type;
        public float rotation;

        public Pattern(string _pattern, StreetType _type, float _rot) {
            this.pattern = _pattern;
            this.type = _type;
            this.rotation = _rot;
        }
    }

    public class MapGenPatterns
    {
        public static Pattern CrossStreetPattern = new Pattern(
            "010" +
            "111" +
            "010",
            StreetType.CrossStreet,
            0
        );

        public static Pattern TStreetDownPattern = new Pattern(
            "000" +
            "111" +
            "010",
            StreetType.TStreet,
            0
        );

        public static Pattern TStreetLeftPattern = new Pattern(
            "010" +
            "110" +
            "010",
            StreetType.TStreet,
            0
        );

        public static Pattern TStreetUpPattern = new Pattern(
            "010" +
            "111" +
            "000",
            StreetType.TStreet,
            0
        );

        public static Pattern TStreetRightPattern = new Pattern(
            "010" +
            "011" +
            "010",
            StreetType.TStreet,
            0
        );

        public static Pattern LStreetUpPattern = new Pattern(
            "010" +
            "011" +
            "000",
            StreetType.TStreet,
            0
        );

        public static Pattern LStreetRightPattern = new Pattern(
            "000" +
            "011" +
            "010",
            StreetType.TStreet,
            0
        );

        public static Pattern LStreetDownPattern = new Pattern(
            "000" +
            "110" +
            "010",
            StreetType.TStreet,
            0
        );

        public static Pattern LStreetLeftPattern = new Pattern(
            "010" +
            "011" +
            "010",
            StreetType.TStreet,
            0
        );
    }
}
