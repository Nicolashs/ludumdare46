﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Street : MonoBehaviour
{
    public Vector2Int positionalId;
    public List<StreetExplorationArea> explorationAreaList;

    [Header("Spawn")]
    public List<Transform> enemySpawnList;
    public List<Transform> itemSpawnList;

    [Header("Debug")]
    bool checkAreaVolumes;

    public ReflectionProbe reflectionProbe;

    Cubemap reflectionCubemap;

    private void Start() {
        reflectionCubemap = new Cubemap(reflectionProbe.resolution, UnityEngine.Experimental.Rendering.DefaultFormat.LDR, UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
        reflectionProbe.customBakedTexture = reflectionCubemap;
        reflectionProbe.RenderProbe();
    }

    private void OnDrawGizmos() {
        /*Gizmos.color = Color.green;
        for (int i = 0; i < explorationAreaList.Count; i++) {
            if (checkAreaVolumes)
                Gizmos.DrawCube(explorationAreaList[i].transform.position, explorationAreaList[i].areaVolumeSize);
            else
                Gizmos.DrawWireCube(explorationAreaList[i].transform.position, explorationAreaList[i].areaVolumeSize);
        }*/

        //Enemy
        Gizmos.color = Color.red;
        foreach (Transform point in enemySpawnList) {
            Gizmos.DrawSphere(point.position, 1);
        }

        //Item
        Gizmos.color = Color.green;
        foreach (Transform point in itemSpawnList) {
            Gizmos.DrawSphere(point.position, 1);
        }
    }
}
