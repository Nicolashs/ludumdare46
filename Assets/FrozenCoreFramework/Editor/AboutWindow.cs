﻿using UnityEditor;
using UnityEngine;

public class AboutWindow : EditorWindow
{
    [MenuItem("FrozenCore/About", false, 1)]
    public static void ShowWindow()
    {
        AboutWindow aboutWindow = EditorWindow.GetWindow<AboutWindow>("About");
    }

    void OnGUI()
    {
        GUILayout.Label("About the FrozenCore framework", EditorStyles.boldLabel);

        GUILayout.Space(20);

        GUILayout.Label("This framework is still in development and have the following features:\n");
        GUILayout.Label("Spawning Pool: Spawns and despawns GameObjects and keep them in cash.");
        GUILayout.Label("State Machine (unfinished): Manage multiple states and triggers the Enter and Exit methods when changing states.");
        GUILayout.Label("Timer: Set an action to be executed after x seconds.");
        GUILayout.Label("Tween Curves and Tween Manager: Create Tweens that use Animation Curves to interpolate values.");

        GUILayout.Space(20);

        GUILayout.Label("Experimental Version");
        GUILayout.Label("Made by Nícolas Hoffmann.");
    }
}