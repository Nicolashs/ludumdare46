﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace FrozenCore
{
    public class EventsWindow : EditorWindow
    {
        private const string eventTypePath = "/FrozenCoreFramework/Scripts/EventManager/EventType.cs";

        private const int eventTypeNameWidth = 200;

        private static EditorWindow window;

        private static string newEventType;
        private static List<string> editableEventTypes;

        #region Initialization Methods
        [MenuItem("FrozenCore/Event Manager", false)]
        public static void ShowWindow()
        {
            Initialize();
        }

        private static void Initialize()
        {
            window = GetWindow<EventsWindow>("Event Manager");

            newEventType = string.Empty;
        }

        private void OnEnable()
        {
            LoadExistingEventTypes();
        }

        private void LoadExistingEventTypes()
        {
            int eventTypesCount = Enum.GetNames(typeof(EventType)).Length;

            editableEventTypes = new List<string>();

            for (int i = 0; i < eventTypesCount; i++)
            {
                EventType iteratedType = (EventType)i;

                editableEventTypes.Add(iteratedType.ToString());
            }
        }
        #endregion

        #region GUI Methods
        private void OnGUI()
        {
            DrawHeaders();
            
            DrawExistingContent();

            GUILayout.Space(10);

            DrawCreatePanel();
        }

        private void DrawHeaders()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Label("EventType Name", EditorStyles.boldLabel, GUILayout.Width(eventTypeNameWidth));

                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Save and Close"))
                    {
                        SaveEnumFile();
                        AssetDatabase.Refresh();
                        window.Close();
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }

        private void DrawCreatePanel()
        {
            GUILayout.Label("New Event Type", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            {
                newEventType = GUILayout.TextField(newEventType, GUILayout.Width(eventTypeNameWidth));

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Add Event Type"))
                {
                    if (DoesNewNameExists(newEventType) == false && newEventType != string.Empty)
                    {
                        editableEventTypes.Add(newEventType);

                        newEventType = string.Empty;
                    }
                    else
                    {
                        Debug.LogWarning("FrozenCore Framework: EventType name already exists or have empty fields.");
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawExistingContent()
        {
            for (int i = 0; i < editableEventTypes.Count; i++)
            {
                string editableEventType = editableEventTypes[i];

                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    editableEventType = GUILayout.TextField(editableEventType, GUILayout.Width(eventTypeNameWidth));

                    editableEventTypes[i] = editableEventType;

                    GUILayout.FlexibleSpace();

                    GUI.color = Color.red;
                    if (GUILayout.Button("Remove"))
                    {
                        editableEventTypes.RemoveAt(i);
                    }
                    GUI.color = Color.white;
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private bool DoesNewNameExists(string newName)
        {
            for (int i = 0; i < editableEventTypes.Count; i++)
            {
                if (newName == editableEventTypes[i])
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region File Methods
        private void SaveEnumFile()
        {
            string path = Application.dataPath + eventTypePath;

            StreamWriter file = new StreamWriter(path);
            file.WriteLine("namespace FrozenCore\n{\n\tpublic enum EventType \n\t{");
            for (int i = 0; i < editableEventTypes.Count; i++)
            {
                file.Write("\t\t" + editableEventTypes[i]);

                if (i < editableEventTypes.Count - 1)
                {
                    file.Write(",\n");
                }
            }
            file.WriteLine("\n\t} \n}");
            file.Close();
        }
        #endregion
    }
}
