﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

namespace FrozenCore
{
    public class TweenCurvesWindow : EditorWindow
    {
        private static TweenCurvesData loadedTweenCurvesData;

        private static List<TweenCurvesData.TweenCurve> editableTweenCurvesList;

        private static string newTweenCurveText;
        private static AnimationCurve newTweenCurve;

        private static EditorWindow window;

        #region Initialization Methods
        [MenuItem("FrozenCore/Tween Curves", false)]
        public static void ShowWindow()
        {
            Initialize();
        }

        private static void Initialize()
        {
            window = GetWindow<TweenCurvesWindow>("Tween Curves");

            newTweenCurveText = string.Empty;
            newTweenCurve = new AnimationCurve();
        }

        private void OnEnable()
        {
            LoadTweenCurvesData();
        }
        #endregion

        #region file methods
        private static void LoadTweenCurvesData()
        {
            loadedTweenCurvesData = AssetDatabase.LoadAssetAtPath(TweenCurvesData.tweenCurvesDataPath, typeof(TweenCurvesData)) as TweenCurvesData;

            if(loadedTweenCurvesData == null)
            {
                loadedTweenCurvesData = ScriptableObject.CreateInstance<TweenCurvesData>();

                loadedTweenCurvesData.tweenCurvesList = new List<TweenCurvesData.TweenCurve>();
                loadedTweenCurvesData.tweenCurvesList.Add(CreateLinearTweenCurve());

                AssetDatabase.CreateAsset(loadedTweenCurvesData, TweenCurvesData.tweenCurvesDataPath);
                AssetDatabase.SaveAssets();
            }

            editableTweenCurvesList = loadedTweenCurvesData.tweenCurvesList;
        }

        private void SaveTweenCurvesData()
        {
            loadedTweenCurvesData.tweenCurvesList = editableTweenCurvesList;

            SaveEnumFile();

            EditorUtility.SetDirty(loadedTweenCurvesData);
            AssetDatabase.Refresh();

            window.Close();
        }

        private void SaveEnumFile()
        {
            string path = Application.dataPath + TweenCurvesData.tweenCurvesTypePath;
        
            StreamWriter file = new StreamWriter(path);
            file.WriteLine("namespace FrozenCore\n{\n\tpublic enum TweenCurvesType \n\t{");
            for (int i = 0; i < editableTweenCurvesList.Count; i++)
            {
                file.Write("\t\t"+editableTweenCurvesList[i].tweenCurveName);

                if (i < editableTweenCurvesList.Count - 1)
                {
                    file.Write(",\n");
                }
            }
            file.WriteLine("\n\t} \n}");
            file.Close();
        }

        private bool DoesNewNameExists(string newName)
        {
            for (int i = 0; i < editableTweenCurvesList.Count; i++)
            {
                if (newName == editableTweenCurvesList[i].tweenCurveName)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion
    
        #region GUI Methods
        private void OnGUI()
        {
            DrawHeaders();

            DrawExistingContent();

            DrawCreatePanel();
        }
    
        private void DrawHeaders()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Tween Curve Name", EditorStyles.boldLabel, GUILayout.Width(200));
                    GUILayout.Label("Tween Curve", EditorStyles.boldLabel, GUILayout.Width(100));

                    GUILayout.FlexibleSpace();
                
                    if (GUILayout.Button("Save and Close"))
                    {

                        SaveTweenCurvesData();
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }

        private void DrawExistingContent()
        {
            if (editableTweenCurvesList.Count == 0)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    GUILayout.FlexibleSpace();
                    GUILayout.Label("No tween curves added.");
                    GUILayout.FlexibleSpace();
                }
                EditorGUILayout.EndHorizontal();
            }

            for (int i = 0; i < editableTweenCurvesList.Count; i++)
            {
                TweenCurvesData.TweenCurve editableTweenCurve = editableTweenCurvesList[i];

                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                {
                    if (editableTweenCurve.tweenCurveName == TweenCurvesType.Linear.ToString())
                    {
                        GUILayout.Label(editableTweenCurve.tweenCurveName, GUILayout.Width(195));
                        GUILayout.Label("unable to edit.", GUILayout.Width(100));

                        EditorGUILayout.EndHorizontal();
                        continue;
                    }

                    editableTweenCurve.tweenCurveName = GUILayout.TextField(editableTweenCurve.tweenCurveName, GUILayout.Width(195));
                    editableTweenCurve.tweenCurve = EditorGUILayout.CurveField(editableTweenCurve.tweenCurve, GUILayout.Width(100));

                    editableTweenCurvesList[i] = editableTweenCurve;
                
                    GUILayout.FlexibleSpace();

                    GUI.color = Color.red;
                    if (GUILayout.Button("Remove"))
                    {
                        editableTweenCurvesList.RemoveAt(i);

                        SaveTweenCurvesData();
                    }
                    GUI.color = Color.white;
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private void DrawCreatePanel()
        {
            GUILayout.Label("New Tween Curve", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            {
                newTweenCurveText = GUILayout.TextField(newTweenCurveText, GUILayout.Width(195));

                newTweenCurve = EditorGUILayout.CurveField(newTweenCurve, GUILayout.Width(100));

                GUILayout.FlexibleSpace();

                if(GUILayout.Button("Add new curve"))
                {
                    if(DoesNewNameExists(newTweenCurveText) == false && newTweenCurveText != string.Empty && newTweenCurve.keys.Length > 0)
                    { 
                        TweenCurvesData.TweenCurve newTweenCurveData = new TweenCurvesData.TweenCurve();
                        newTweenCurveData.tweenCurveName = newTweenCurveText;
                        newTweenCurveData.tweenCurve = newTweenCurve;

                        editableTweenCurvesList.Add(newTweenCurveData);

                        newTweenCurveText = string.Empty;
                        newTweenCurve = null;// new AnimationCurve();

                        SaveTweenCurvesData();
                    }
                    else
                    {
                        Debug.LogWarning("FrozenCore Framework: Tween curve name already exists or have empty fields.");
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region Other Methods
        private static TweenCurvesData.TweenCurve CreateLinearTweenCurve()
        {
            Keyframe[] linearKeyFrames = new Keyframe[2];

            for (int i = 0; i < 2; i++)
            {
                linearKeyFrames[i].time = i;
                linearKeyFrames[i].value = i;
                linearKeyFrames[i].tangentMode = 34;
                linearKeyFrames[i].inTangent = 1;
                linearKeyFrames[i].outTangent = 1;
            }

            AnimationCurve linearAnimationCurve = new AnimationCurve(linearKeyFrames);

            TweenCurvesData.TweenCurve linearTweenCurve = new TweenCurvesData.TweenCurve();
            linearTweenCurve.tweenCurve = linearAnimationCurve;
            linearTweenCurve.tweenCurveName = "Linear";

            return linearTweenCurve;
        }
        #endregion
    }
}