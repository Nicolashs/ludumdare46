﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrozenCore
{
    public class EventManager
    {
        private Dictionary<string, Action> triggerEvents;
        private Dictionary<string, Action<EventData>> messageEvents;

        private static EventManager instance;
        public static EventManager Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.LogError("FrozenCore Framework: Event Manager disabled.");
                }

                return instance;
            }
        }

        public void Initialize()
        {
            instance = this;

            triggerEvents = new Dictionary<string, Action>();
            messageEvents = new Dictionary<string, Action<EventData>>();
        }

        public void AddEventCallback(string eventName, Action callback)
        {
            if (triggerEvents.ContainsKey(eventName) == false)
            {
                triggerEvents.Add(eventName, callback);
            }
            else
            {
                triggerEvents[eventName] += callback;
            }
        }

        public void AddEventCallback(string eventName, Action<EventData> callback)
        {
            if (messageEvents.ContainsKey(eventName) == false)
            {
                messageEvents.Add(eventName, callback);
            }
            else
            {
                messageEvents[eventName] += callback;
            }
        }

        public void RemoveEventCallback(string eventName, Action callback)
        {
            if (triggerEvents.ContainsKey(eventName) == true)
            {
                triggerEvents[eventName] -= callback;
            }
        }

        public void RemoveEventCallback(string eventName, Action<EventData> callback)
        {
            if (messageEvents.ContainsKey(eventName) == true)
            {
                messageEvents[eventName] -= callback;
            }
        }

        public void CallEvent(string eventName)
        {
            if (triggerEvents.ContainsKey(eventName) == true)
            {
                triggerEvents[eventName]?.Invoke();
            }
        }

        public void CallEvent(string eventName, EventData eventData)
        {
            if (messageEvents.ContainsKey(eventName) == true)
            {
                messageEvents[eventName]?.Invoke(eventData);
            }
        }
    }
}
