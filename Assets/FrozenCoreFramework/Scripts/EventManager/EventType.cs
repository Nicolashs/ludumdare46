namespace FrozenCore
{
	public enum EventType 
	{
		IntEvent,
		FloatEvent,
		StringEvent
	} 
}
