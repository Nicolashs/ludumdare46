﻿using UnityEditor;
using UnityEngine;
using System.Collections;

namespace FrozenCore
{
    public class FrozenCoreManager : Singleton<MonoBehaviour>
    {
        #region Attributes
        private static FrozenCoreManager instance;
        
        private TweenManager tweenManager = null;

        private TimerManager timerManager = null;

        private EventManager eventManager = null;
        #endregion

        #region Methods
        public void Initialize()
        {
            tweenManager = new TweenManager();
            tweenManager.Initialize();

            timerManager = new TimerManager();
            timerManager.Initialize();

            eventManager = new EventManager();
            eventManager.Initialize();
        }     
        #endregion

        #region Unity Methods
        private void Awake()
        {
            Initialize();
        }

        private void Update()
        {
            tweenManager.Update();
            timerManager.Update();
        }
        #endregion
    }
}
