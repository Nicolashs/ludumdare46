﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using FrozenCore;

public static class FadeExtension
{
    public static Tween FadeIn(this Graphic graphic, float time, float alpha = 1f)
    {
        Tween fadeInTween = new Tween(0, alpha, time, TweenCurvesType.Linear, (newAlpha) =>
        {
            if (graphic != null)
            {
                Color newColor = graphic.color;
                newColor.a = newAlpha;
                graphic.color = newColor;
            }
            else
            {
                return;
            }
        });

        return fadeInTween;
    }

    public static Tween FadeIn(this CanvasGroup group, float time, float alpha = 1f)
    {
        Tween fadeInTween = new Tween(0, alpha, time, TweenCurvesType.Linear, (newAlpha) =>
        {
            if (group != null)
            {
                group.alpha = newAlpha;
            }
            else
            {
                return;
            }
        });

        return fadeInTween;
    }

    public static Tween FadeIn(this Material material, float time)
    {
        Tween fadeInTween = new Tween(0f, 1f, time, TweenCurvesType.Linear, (newAlpha) =>
        {
            if (material != null)
            {
                Color newColor = material.color;
                newColor.a = newAlpha;
                material.color = newColor;
            }
            else
            {
                return;
            }
        });

        return fadeInTween;
    }

    public static Tween FadeIn(this SpriteRenderer material, float time)
    {
        Tween fadeInTween = new Tween(0f, 1f, time, TweenCurvesType.Linear, (newAlpha) =>
        {
            if (material != null)
            {
                Color newColor = material.color;
                newColor.a = newAlpha;
                material.color = newColor;
            }
            else
            {
                return;
            }
        });

        return fadeInTween;
    }

    public static Tween FadeOut(this Graphic graphic, float time)
    {
        Tween fadeOutTween = new Tween(graphic.color.a, 0f, time, TweenCurvesType.Linear, (newAlpha) =>
        {
            if (graphic != null)
            {
                Color newColor = graphic.color;
                newColor.a = newAlpha;
                graphic.color = newColor;
            }
            else
            {
                return;
            }
        });

        return fadeOutTween;
    }

    public static Tween FadeOut(this CanvasGroup group, float time)
    {
        Tween fadeOutTween = new Tween(group.alpha, 0f, time, TweenCurvesType.Linear, (newAlpha) =>
        {
            if (group != null)
            {
                group.alpha = newAlpha;
            }
            else
            {
                return;
            }
        });

        return fadeOutTween;
    }

    public static Tween FadeOut(this Material material, float time)
    {
        Tween fadeOutTween = new Tween(material.color, Color.clear, time, TweenCurvesType.Linear, (newColor) =>
        {
            if (material != null)
            {
                material.color = newColor;
            }
            else
            {
                return;
            }
        });

        return fadeOutTween;
    }

    public static Tween FadeOut(this SpriteRenderer material, float time)
    {
        Tween fadeOutTween = new Tween(material.color.a, 0f, time,  TweenCurvesType.Linear, (alpha) =>
        {
            if (material != null)
            {
                Color newColor = material.color;
                newColor.a = alpha;
                material.color = newColor;
            }
            else
            {
                return;
            }
        });

        return fadeOutTween;
    }
}
