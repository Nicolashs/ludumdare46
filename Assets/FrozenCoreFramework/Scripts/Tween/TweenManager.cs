﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace FrozenCore
{
    public class TweenManager
    {
        #region Attributes
        private static TweenManager instance;
        public static TweenManager Instance
        {
            get
            {
                if(instance == null)
                {
                    Debug.LogError("FrozenCore Framework: Tween Manager disabled.");
                }
                
                return instance;
            }
        }

        private List<Tween> runningTweensList;

        private Dictionary<TweenCurvesType, AnimationCurve> tweenCurves;
        #endregion

        #region Methods
        public void Initialize()
        {
            instance = this;

            tweenCurves = new Dictionary<TweenCurvesType, AnimationCurve>();
            
            runningTweensList = new List<Tween>();

            LoadTweenCurvesData();
        }

        private void LoadTweenCurvesData()
        {
            TweenCurvesData loadedTweenCurvesData = Resources.Load("Data/TweenCurvesData") as TweenCurvesData;

            List<TweenCurvesData.TweenCurve> loadedTweenCurvesList = loadedTweenCurvesData.tweenCurvesList;

            for (int i = 0; i < loadedTweenCurvesList.Count; i++)
            {
                TweenCurvesType type = (TweenCurvesType)Enum.Parse(typeof(TweenCurvesType), loadedTweenCurvesList[i].tweenCurveName);

                tweenCurves.Add(type, loadedTweenCurvesList[i].tweenCurve);
            }
        }

        public void Update()
        {
            for (int i = 0; i < runningTweensList.Count; i++)
            {
                runningTweensList[i].UpdateTween();
            }
        }
        
        public void AddTween(Tween tween)
        {
            runningTweensList.Add(tween);
        }
        
        public void RemoveTween(Tween tween)
        {
            runningTweensList.Remove(tween);
        }

        public AnimationCurve GetAnimationCurve(TweenCurvesType easing)
        {
            return tweenCurves[easing];
        }
        #endregion
    }
}
