namespace FrozenCore
{
	public enum TweenCurvesType 
	{
		Linear,
		EaseIn,
		EaseOut,
		ElasticOut,
		ElasticIn
	} 
}
