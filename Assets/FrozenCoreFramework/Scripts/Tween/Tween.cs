﻿using UnityEngine;
using System;
using System.Collections;

namespace FrozenCore
{
    public class Tween
    {
        #region Tween Interpolation
        private abstract class TweenInterpolation
        {
            public abstract void InterpolateValue(float curveValue);
        }

        private class FloatTweenInterpolation : TweenInterpolation
        {
            public float from;
            public float to;
            public Action<float> Callback;

            public override void InterpolateValue(float curveValue)
            {
                float floatInterpolatedValue = from + ((to - from) * curveValue);

                Callback(floatInterpolatedValue);
            }
        }

        private class Vector3TweenInterpolation : TweenInterpolation
        {
            public Vector3 from;
            public Vector3 to;
            public Action<Vector3> Callback;

            public override void InterpolateValue(float curveValue)
            {
                Vector3 vector3InterpolatedValue = new Vector3();
                vector3InterpolatedValue.x = from.x + ((to.x - from.x) * curveValue);
                vector3InterpolatedValue.y = from.y + ((to.y - from.y) * curveValue);
                vector3InterpolatedValue.z = from.z + ((to.z - from.z) * curveValue);

                Callback(vector3InterpolatedValue);
            }
        }

        private class ColorTweenInterpolation : TweenInterpolation
        {
            public Color from;
            public Color to;
            public Action<Color> Callback;

            public override void InterpolateValue(float curveValue)
            {
                Color colorInterpolatedValue = new Color();
                colorInterpolatedValue.r = from.r + ((to.r - from.r) * curveValue);
                colorInterpolatedValue.g = from.g + ((to.g - from.g) * curveValue);
                colorInterpolatedValue.b = from.b + ((to.b - from.b) * curveValue);
                colorInterpolatedValue.a = from.a + ((to.a - from.a) * curveValue);

                Callback(colorInterpolatedValue);
            }
        }
        #endregion

        #region TweenState
        public enum TweenState
        {
            Running, Paused, Completed, Stopped
        }
        #endregion

        #region Attributes
        private TweenInterpolation tweenInterpolation;
        
        private float durationTime;
        private float elapsedTime;

        private bool isLoop;
        private bool destroyOnComplete;

        private AnimationCurve curve;

        public Action OnComplete;

        public TweenState State { get; private set; }
        
        public bool IsLoop
        {
            set
            {
                isLoop = true;
                destroyOnComplete = false;
            }
        }

        public bool DestroyOnComplete
        {
            set
            {
                destroyOnComplete = true;
                isLoop = false;
            }
        }
        #endregion

        #region Methods
        public Tween(float from, float to, float durationTime, TweenCurvesType easing, Action<float> Callback)
        {
            this.durationTime = durationTime;
            isLoop = false;
            destroyOnComplete = true;

            tweenInterpolation = new FloatTweenInterpolation();

            ((FloatTweenInterpolation)tweenInterpolation).from = from;
            ((FloatTweenInterpolation)tweenInterpolation).to = to;
            ((FloatTweenInterpolation)tweenInterpolation).Callback = Callback;

            curve = TweenManager.Instance.GetAnimationCurve(easing);

            StartTween();
        }
        
        public Tween(Vector3 from, Vector3 to, float durationTime, TweenCurvesType easing, Action<Vector3> Callback)
        {
            this.durationTime = durationTime;
            isLoop = false;
            destroyOnComplete = true;

            tweenInterpolation = new Vector3TweenInterpolation();

            ((Vector3TweenInterpolation)tweenInterpolation).from = from;
            ((Vector3TweenInterpolation)tweenInterpolation).to = to;
            ((Vector3TweenInterpolation)tweenInterpolation).Callback = Callback;

            curve = TweenManager.Instance.GetAnimationCurve(easing);

            StartTween();
        }

        public Tween(Color from, Color to, float durationTime, TweenCurvesType easing, Action<Color> Callback)
        {
            this.durationTime = durationTime;
            isLoop = false;
            destroyOnComplete = true;

            tweenInterpolation = new ColorTweenInterpolation();

            ((ColorTweenInterpolation)tweenInterpolation).from = from;
            ((ColorTweenInterpolation)tweenInterpolation).to = to;
            ((ColorTweenInterpolation)tweenInterpolation).Callback = Callback;

            curve = TweenManager.Instance.GetAnimationCurve(easing);

            StartTween();
        }

        public void StartTween()
        {
            elapsedTime = 0;

            State = TweenState.Running;

            TweenManager.Instance.AddTween(this);
        }

        public void PauseTween()
        {
            if(State != TweenState.Completed)
            {
                State = TweenState.Paused;
            }
        }

        public void ResumeTween()
        {
            if(State == TweenState.Paused)
            {
                State = TweenState.Running;
            }
        }

        public void StopTween()
        {
            elapsedTime = 0;
            State = TweenState.Stopped;

            TweenManager.Instance.RemoveTween(this);
        }

        public void UpdateTween()
        {
            switch (State)
            {
                case TweenState.Running:

                    elapsedTime += Time.deltaTime;
        
                    if(elapsedTime >= durationTime)
                    {
                        State = TweenState.Completed;

                        elapsedTime = durationTime;
                    }

                    tweenInterpolation.InterpolateValue(curve.Evaluate(elapsedTime / durationTime));
                    
                    break;
                case TweenState.Paused:
                    break;
                case TweenState.Completed:
                    OnComplete?.Invoke();

                    if(isLoop == true)
                    {
                        StartTween();
                    }
                    else if(destroyOnComplete == true)
                    {
                        TweenManager.Instance.RemoveTween(this);
                    }

                    break;
                case TweenState.Stopped:
                    break;
            }
        }
        #endregion
    }
}
