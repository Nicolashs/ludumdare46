﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace FrozenCore
{
    public class TweenCurvesData : ScriptableObject
    {
        [Serializable]
        public struct TweenCurve
        {
            public string tweenCurveName;
            public AnimationCurve tweenCurve;
        }

        public const string tweenCurvesDataPath = "Assets/FrozenCoreFramework/Resources/Data/TweenCurvesData.asset";
        public const string tweenCurvesTypePath = "/FrozenCoreFramework/Scripts/Tween/TweenCurvesType.cs";
        
        public List<TweenCurve> tweenCurvesList;
    }
}