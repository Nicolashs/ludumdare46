﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FrozenCore
{
    public class TimerManager
    {
        #region Attributes
        private static TimerManager instance;
        public static TimerManager Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.LogError("FrozenCore Framework: Timer Manager disabled.");
                }

                return instance;
            }
        }

        private List<Timer> timersList;
        #endregion

        #region Methods
        public void Initialize()
        {
            instance = this;

            timersList = new List<Timer>();
        }

        public void Update()
        {
            for (int i = 0; i < timersList.Count; i++)
            {
                timersList[i].UpdateTimer();
            }
        }
        
        public void AddTimer(Timer timer)
        {
            timersList.Add(timer);
        }

        public void RemoveTimer(Timer timer)
        {
            timersList.Remove(timer);
        }
        #endregion
    }
}
