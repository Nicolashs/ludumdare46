﻿using UnityEngine;
using System;
using System.Collections;

namespace FrozenCore
{
    public class Timer
    {
        #region TimerState
        private enum TimerState
        {
            Running, Paused, Completed, Stopped
        }
        #endregion

        #region Attributes
        private float durationTime;
        private float elapsedTime;

        private Action Callback;

        private TimerState state;
        #endregion

        #region Methods
        public Timer(float durationTime, Action Callback)
        {
            this.durationTime = durationTime;
            this.Callback = Callback;
            
            StartTimer();
        }
        
        public void StartTimer()
        {
            elapsedTime = 0;

            state = TimerState.Running;

            TimerManager.Instance.AddTimer(this);
        }

        public void PauseTimer()
        {
            state = TimerState.Paused;
        }

        public void ResumeTimer()
        {
            if(state == TimerState.Paused)
            {
                state = TimerState.Running;
            }
        }

        public void StopTimer()
        {
            elapsedTime = 0;
            state = TimerState.Stopped;

            TimerManager.Instance.RemoveTimer(this);
        }

        public void UpdateTimer()
        {
            switch (state)
            {
                case TimerState.Running:
                    elapsedTime += Time.deltaTime;
                    
                    if (elapsedTime >= durationTime)
                    {
                        state = TimerState.Completed;

                        elapsedTime = durationTime;
                    }
                    break;
                case TimerState.Paused:
                    break;
                case TimerState.Completed:
                    Callback?.Invoke();

                    TimerManager.Instance.RemoveTimer(this);
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
